FROM python:3.7-alpine

ENV TERRAFORM_VERSION 0.12.20

RUN apk --no-cache update && \
    apk --no-cache add py3-setuptools ca-certificates curl groff less jq \
    openssh-client curl unzip && pip --no-cache-dir install awscli && \
    curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    -o terraform.zip && unzip terraform.zip && mv terraform /usr/local/bin && mkdir /root/.ssh && \
    curl -LO $(curl -sL https://api.github.com/repos/mozilla/sops/releases/latest | jq -r '.assets[].browser_download_url' | grep linux) && \
    mv sops-v*.linux /usr/local/bin/sops && chmod +x /usr/local/bin/sops && \
    mkdir -p /root/.terraform.d/plugins && \
    curl -LO $(curl -sL https://api.github.com/repos/carlpett/terraform-provider-sops/releases/latest | jq -r '.assets[].browser_download_url' | grep linux_amd64) && \
    unzip terraform-provider-sops_*_linux_amd64.zip && mv terraform-provider-sops_* /root/.terraform.d/plugins/ && \
    chmod +x /root/.terraform.d/plugins/terraform-provider-sops_v* && rm -rf /var/cache/apk/*
